from django.db import models, transaction
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from decimal import Decimal
import datetime
from .errors import InsufficientFunds
import uuid

# Celery
import json
from django.utils import timezone
from django_enum_choices.fields import EnumChoiceField
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from .enums import TimeInterval, SetupStatus


def random_id():
    return "DK-" + str(uuid.uuid4())


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account_name = models.CharField(max_length=40)
    is_loan = models.BooleanField(default=False)
    account_id = models.CharField(default=random_id, max_length=100)

    @property
    def movements(self) -> QuerySet:
        return Ledger.objects.filter(account=self)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum("amount"))["amount__sum"] or Decimal(
            0
        )

    def __str__(self):
        return f"{self.user} :: {self.account_name} :: {self.pk}"


class Ledger(models.Model):
    transaction_id = models.CharField(max_length=100)
    account = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    text = models.CharField(max_length=100)
    date = models.DateField(default=datetime.date.today)

    @classmethod
    def transfer(
        cls,
        amount,
        debit_account,
        debit_text,
        credit_account,
        credit_text,
        is_loan=False,
    ) -> int:
        assert amount >= 0, "Negative amount not allowed for transfer."
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                id = uuid.uuid4()
                # cls is the Ledger object
                cls(
                    transaction_id=id,
                    account=credit_account,
                    amount=amount,
                    text=credit_text,
                ).save()
                cls(
                    transaction_id=id,
                    account=debit_account,
                    amount=-amount,
                    text=debit_text,
                ).save()
                print("transaction complete")
            else:
                raise InsufficientFunds
        return id

    @classmethod
    def bank_to_bank_transfer(
        cls,
        amount,
        debit_account,
        debit_text,
        credit_account,
        credit_text,
        transaction_id,
    ) -> int:
        assert amount >= 0, "Negative amount not allowed for transfer."
        with transaction.atomic():
            if debit_account.balance >= amount:
                cls(
                    transaction_id=transaction_id,
                    account=credit_account,
                    amount=amount,
                    text=credit_text,
                ).save()
                cls(
                    transaction_id=transaction_id,
                    account=debit_account,
                    amount=-amount,
                    text=debit_text,
                ).save()
                print("transaction complete")
            else:
                raise InsufficientFunds
        return transaction_id

    def __str__(self):
        return f"{self.amount} :: {self.transaction_id} :: {self.date} :: {self.account} :: {self.text}"


class ScheduledPayment(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=70, blank=False)
    status = EnumChoiceField(SetupStatus, default=SetupStatus.active)
    created_at = models.DateTimeField(auto_now_add=True)
    time_interval = EnumChoiceField(TimeInterval, default=TimeInterval.one_min)
    from_account = models.ForeignKey(
        Account, related_name="FromAccount", null=True, on_delete=models.CASCADE
    )
    to_account = models.ForeignKey(
        Account, related_name="ToAccount", null=True, on_delete=models.CASCADE
    )
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    text = models.CharField(max_length=100)
    text2 = models.CharField(max_length=100)

    task = models.OneToOneField(
        PeriodicTask, on_delete=models.CASCADE, null=True, blank=True
    )

    def delete(self, *args, **kwargs):
        if self.task is not None:
            self.task.delete()

        return super(self.__class__, self).delete(*args, **kwargs)

    def setup_task(self):

        self.task = PeriodicTask.objects.create(
            name=self.title,
            task="sp_task",
            interval=self.interval_schedule,
            args=json.dumps([self.id]),
            start_time=timezone.now(),
        )
        self.save()

    @property
    def interval_schedule(self):
        if self.time_interval == TimeInterval.one_min:
            return IntervalSchedule.objects.get(every=1, period="minutes")
        if self.time_interval == TimeInterval.five_mins:
            return IntervalSchedule.objects.get(every=5, period="minutes")
        if self.time_interval == TimeInterval.one_hour:
            return IntervalSchedule.objects.get(every=1, period="hours")

        raise NotImplementedError(
            """Interval Schedule for {interval} is not added.""".format(
                interval=self.time_interval.value
            )
        )

    def __str__(self):
        return f"{self.user} :: {self.title} "
