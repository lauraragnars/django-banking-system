from django.contrib import admin
from .models import Account, Ledger, ScheduledPayment

admin.site.register(Account)
admin.site.register(Ledger)
admin.site.register(ScheduledPayment)
