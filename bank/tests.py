# from doctest import testsource
from django.test import TestCase
from django.contrib.auth.models import User
from bank.models import Account, Ledger


class BankTestCase(TestCase):
    def setUp(self):
        User(username="testUserBasic").save()
        User(username="testUserSilver").save()

    def test_create_account(self):
        testUserBasic = User.objects.get(username="testUserBasic")
        Account(
            user=testUserBasic, account_name="TestCase Account", is_loan=False
        ).save()

    def test_can_take_loan(self):
        testUserBasic = User.objects.get(username="testUserBasic")
        testUserSilver = User.objects.get(username="testUserSilver")
        testUserBasic.profile.customer_rank = "basic"
        testUserSilver.profile.customer_rank = "silver"
        testUsers = [testUserBasic, testUserSilver]
        for i in testUsers:
            if i.profile.customer_rank == "basic":
                assert i.profile.can_take_loan is False
            else:
                assert i.profile.can_take_loan is True

    def test_make_loan(self):
        testUserSilver = User.objects.get(username="testUserSilver")
        testUserSilver.profile.customer_rank = "silver"
        testLoanAccount = Account(
            user=testUserSilver, account_name="AccountToInsertLoanTo", is_loan=True
        ).save()
        testUserSilver.profile.take_loan(
            testUserSilver, testLoanAccount, "Test loan", 2000, "Loan account name"
        )

    def test_internal_transfer(self):
        # Creating user
        testUser = User.objects.get(username="testUserSilver")
        testUser.profile.customer_rank = "silver"

        # Creating two accounts
        Account(user=testUser, account_name="debitAccount", is_loan=False).save()
        Account(user=testUser, account_name="creditAccount", is_loan=False).save()
        debitAccount = Account.objects.get(account_name="debitAccount")
        creditAccount = Account.objects.get(account_name="creditAccount")

        # Inserting funds to debit account
        testUser.profile.take_loan(
            testUser, debitAccount, "Loan to debitaccount", 2000, "Loan account name"
        )

        # Transferring from debitAccount to creditAccount
        Ledger(
            transaction_id=1, account=creditAccount, amount=200, text="credit_text"
        ).save()
        Ledger(
            transaction_id=1, account=debitAccount, amount=-200, text="debit_text"
        ).save()
        Ledger.transfer(
            200, debitAccount, "debitAccount text", creditAccount, "creditAccount text"
        )
        # print("debitAccount balance: ", debitAccount.balance)
        # print("creditAccount balance: ", creditAccount.balance)
