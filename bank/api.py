from rest_framework import generics
from rest_framework.response import Response
from .serializers import GetAccountSerializer
from .models import Ledger, Account


class GetAccount(generics.ListCreateAPIView):
    def get(self, request):
        accounts = Account.objects.all()
        account_id = request.query_params.get("account_id")

        try:
            account = accounts.get(account_id=account_id)
            seralizer = GetAccountSerializer(account, many=False)
            return Response(data=seralizer.data, status=200)
        except Exception:
            return Response({"error": "Could not find account"}, status=500)


class TransferMoney(generics.ListCreateAPIView):
    def post(self, request):
        # all_ledgers = Ledger.objects.all()
        accounts = Account.objects.all()
        account_id = request.query_params.get("account_id")
        transaction_id = request.query_params.get("transaction_id")
        amount = int(request.query_params.get("amount"))
        text = request.query_params.get("text")
        account = accounts.get(account_id=account_id)

        try:
            bank_account = accounts.get(account_name="Bank OPS Account")
            Ledger.bank_to_bank_transfer(
                amount=amount,
                debit_account=bank_account,
                debit_text=text,
                credit_account=account,
                credit_text=text,
                transaction_id=transaction_id,
            )
            return Response({"info": "Transaction complete"}, status=200)
        except Exception:
            return Response({"error": "Transaction failed"}, status=500)


class RollbackTransaction(generics.ListCreateAPIView):
    def post(self, request):

        transaction_id = int(request.query_params.get("transaction_id"))

        try:
            row = Ledger.objects.get(transaction_id=transaction_id)
            row.delete()
            return Response({"info": "transaction deleted"}, status=200)
        except Exception:
            return Response({"error": "Transaction rollback failed"}, status=500)
