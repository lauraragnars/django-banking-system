from django.shortcuts import render
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Account, Ledger, ScheduledPayment
from login.models import Profile
from .enums import SetupStatus
import uuid
import requests
from twilio.rest import Client
import environ
from datetime import datetime


@login_required
def index(request):
    logged_in_user = request.user
    accounts = Account.objects.filter(user=logged_in_user)
    scheduled_payments = ScheduledPayment.objects.filter(user=logged_in_user)
    context = {"accounts": accounts, "scheduled_payments": scheduled_payments}

    if request.method == "POST":

        if "add_scheduled_payment" in request.POST:
            scheduled_payment = ScheduledPayment()
            scheduled_payment.title = request.POST["scheduled_payment_title"]
            scheduled_payment.user = logged_in_user
            scheduled_payment.created_at = datetime
            scheduled_payment.from_account = accounts.get(
                pk=request.POST["sp_accounts"]
            )
            scheduled_payment.to_account = accounts.get(pk=request.POST["sp_accounts2"])
            scheduled_payment.amount = int(request.POST["scheduled_payment_amount"])
            scheduled_payment.text = request.POST["sp_text"]
            scheduled_payment.text2 = request.POST["sp_text2"]
            scheduled_payment.save()
            scheduled_payment.setup_task()
            scheduled_payment.task.enabled = (
                scheduled_payment.status == SetupStatus.active
            )
            scheduled_payment.task.save()

        elif "create_account" in request.POST:
            account = Account()
            account.account_name = request.POST["account_name"]
            account.user = logged_in_user
            account.save()

    return render(request, "bank/index.html", context)


@login_required
def employee_index(request):
    all_customers = User.objects.filter(is_staff=False)
    context = {"customers": all_customers}

    if request.method == "POST":
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        user_name = request.POST["user"]
        phone_number = request.POST["phone_number"]
        email = request.POST["email"]

        if password == confirm_password:
            user = User.objects.create_user(user_name, email, password)
            user.profile.phone_number = phone_number
            user.is_staff = False
            try:
                user.save()
                context = {"message": "Customer created", "customers": all_customers}
            except IntegrityError:
                context = {
                    "error": "Could not create customer",
                    "customers": all_customers,
                }
                # laga þetta, finna ut hvernig að maður lætur síðuna ekki sýna allan errorinn
                raise Exception("Could not create customer")
        else:
            context = {
                "error": "Passwords did not match. Please try again.",
                "customers": all_customers,
            }

    return render(request, "bank/index-employee.html", context)


@login_required
def account_details(request, pk):

    account = Account.objects.get(pk=pk)
    context = {"account": account}

    return render(request, "bank/account-details.html", context)


@login_required
def customer_details(request, pk):

    customer = User.objects.get(pk=pk)
    profile = Profile.objects.get(user=customer)
    customer_accounts = Account.objects.filter(user=customer)

    context = {
        "customer": customer,
        "profile": profile,
        "customer_accounts": customer_accounts,
    }

    if request.method == "POST":
        if "change_rank" in request.POST:
            new_rank = request.POST["rank"]
            print(profile.customer_rank)
            profile.customer_rank = new_rank
            profile.save()

        if "create_customer_account" in request.POST:
            account = Account()
            account.account_name = request.POST["account_name"]
            account.user = customer
            account.save()

    return render(request, "bank/customer-details.html", context)


@login_required
def transaction_details(request):
    pass


@login_required
def get_loan(request):
    context = {}
    logged_in_user = request.user
    accounts = Account.objects.filter(user=logged_in_user)
    context = {"accounts": accounts}

    if request.method == "POST":

        if "make_loan" in request.POST:
            to_loan_account = accounts.get(pk=request.POST["accounts3"])
            from_transfer_text = request.POST["loan_text"]
            loan_amount = int(request.POST["loan_amount"])
            loan_name = request.POST["loan_name"]

            Profile.take_loan(
                logged_in_user,
                to_loan_account,
                from_transfer_text,
                loan_amount,
                loan_name,
            )

    return render(request, "bank/loan.html", context)


# transfer money between your own accounts
@login_required
def transfer_money(request):
    context = {}
    logged_in_user = request.user
    accounts = Account.objects.filter(user=logged_in_user)
    context = {"accounts": accounts}

    if request.method == "POST":
        if "transfer_money" in request.POST:
            from_account = accounts.get(pk=request.POST["accounts"])
            to_account = accounts.get(pk=request.POST["accounts2"])
            from_transfer_text = request.POST["transfer_text"]
            to_transfer_text2 = request.POST["transfer_text2"]
            transfer_amount = int(request.POST["transfer_amount"])

            Ledger.transfer(
                transfer_amount,
                from_account,
                from_transfer_text,
                to_account,
                to_transfer_text2,
            )

    return render(request, "bank/transfer-money.html", context)


# transfer money to another person in the same bank
@login_required
def internal_transfer(request):
    context = {}
    logged_in_user = request.user
    accounts = Account.objects.filter(user=logged_in_user)
    context = {"accounts": accounts}

    if request.method == "POST":
        if "internal_transfer_money" in request.POST:
            accounts = Account.objects.all()
            from_account = accounts.get(pk=request.POST["from_internal_account"])
            to_account_id = request.POST["to_internal_account"]
            from_transfer_text = request.POST["internal_transfer_text"]
            to_transfer_text2 = request.POST["internal_transfer_text2"]
            transfer_amount = int(request.POST["internal_transfer_amount"])

            env = environ.Env()
            environ.Env.read_env()
            account_sid = env("TWILIO_ACCOUNT_SID")
            auth_token = env("TWILIO_AUTH_TOKEN")
            client = Client(account_sid, auth_token)

            try:
                to_account = accounts.get(account_id=to_account_id)
                message = f"From KEA Bank. You have just recived {transfer_amount} from {request.user.username} on your account: {to_account.account_name}. Message from sender: {to_transfer_text2}"
                Ledger.transfer(
                    transfer_amount,
                    from_account,
                    from_transfer_text,
                    to_account,
                    to_transfer_text2,
                )

                message = client.messages.create(
                    from_="+4552513028",
                    body=message,
                    to=to_account.user.profile.phone_number,
                )

            except Exception:
                context = {
                    "accounts": accounts,
                    "error": "Cannot find recipient account",
                }

    return render(request, "bank/internal-transfer.html", context)


# transfer money to another person in another bank
@login_required
def bank_to_bank_transfer(request):
    context = {}
    logged_in_user = request.user
    accounts = Account.objects.filter(user=logged_in_user)
    context = {"accounts": accounts}
    all_accounts = Account.objects.all()

    if request.method == "POST":
        if "bank_transfer" in request.POST:
            internal_from_account = accounts.get(pk=request.POST["from_account"])
            account_id = request.POST["account_id"]
            amount = int(request.POST["bank_transfer_amount"])
            text = request.POST["bank_transfer_text"]
            transaction_id = uuid.uuid4()

            check_account_url = (
                "http://127.0.0.1:4000/bank/api/v1/get-account/?account_id=%s"
                % account_id
            )
            transfer_url = f"http://127.0.0.1:4000/bank/api/v1/transfer/?transaction_id={transaction_id}&amount={amount}&text={text}&account_id={account_id}"
            rollback_url = f"http://127.0.0.1:4000/bank/api/v1/rollback-transaction/?transaction_id={transaction_id}"

            account_response = requests.get(check_account_url)

            if account_response.ok:
                print("response ok")
                transfer_response = requests.post(transfer_url)
                if transfer_response.ok:
                    try:
                        bank_account = all_accounts.get(account_name="Bank IPO Account")
                        print(bank_account, "bank account")
                        Ledger.bank_to_bank_transfer(
                            amount=amount,
                            debit_account=internal_from_account,
                            debit_text=text,
                            credit_account=bank_account,
                            credit_text=text,
                            transaction_id=transaction_id,
                        )

                    except Exception:
                        rollback_response = requests.post(rollback_url)
                        if rollback_response.ok:
                            print("rolled back transaction")
            else:
                print("Cannot find account")

    return render(request, "bank/bank-to-bank-transfer.html", context)
