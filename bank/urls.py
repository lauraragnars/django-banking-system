from django.urls import path, include
from .api import GetAccount, TransferMoney, RollbackTransaction

from . import views

app_name = "bank"

urlpatterns = [
    path("", views.index, name="index"),
    path("index-employee.html", views.employee_index, name="employee_index"),
    path("loan/", views.get_loan, name="loan"),
    path(
        "bank_to_bank_transfer/",
        views.bank_to_bank_transfer,
        name="bank_to_bank_transfer",
    ),
    path("transfer_money/", views.transfer_money, name="transfer_money"),
    path("internal_transfer/", views.internal_transfer, name="internal_transfer"),
    path("account_details/<int:pk>/", views.account_details, name="account_details"),
    path("customer_details/<int:pk>/", views.customer_details, name="customer_details"),
    path("api/v1/get-account/", GetAccount.as_view()),
    path("api/v1/transfer/", TransferMoney.as_view()),
    path("api/v1/rollback-transaction/", RollbackTransaction.as_view()),
    path("api/v1/rest-auth/", include("dj_rest_auth.urls")),
]
