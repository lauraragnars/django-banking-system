from celery import shared_task

from .models import ScheduledPayment, Ledger


@shared_task(name="sp_task")
def sp_task(setup_id):

    setup = ScheduledPayment.objects.get(id=setup_id)
    from_account = setup.from_account
    to_account = setup.to_account
    transfer_amount = int(setup.amount)
    from_transfer_text = setup.text
    to_transfer_text2 = setup.text2

    Ledger.transfer(
        transfer_amount, from_account, from_transfer_text, to_account, to_transfer_text2
    )
