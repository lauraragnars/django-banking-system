from rest_framework import serializers

from .models import Account


class GetAccountSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            "account_id",
            "account_name",
        )
        model = Account
