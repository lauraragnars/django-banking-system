---
title: Getting started
weight: 1
---
# Django banking system / KEA Exam project 

## Setup the project
### To be able pull the Docker image from the container registry run the following command
#### Replace <GITLAB_CI_TOKEN> with the corresponding environment variable on Gitlab

```
docker login -u gitlab-ci-token -p "<GITLAB_CI_TOKEN>" "registry.gitlab.com/lauraragnars/django-banking-system"
```
---
### To run the project on a server
---
#### 1. Create a py-env folder in your home folder

```
$ mkdir ~/py-env
$ python3 -m venv py-env/banking-system
```

#### 2. Activate the environment from the project folder

```
$ source ~/py-env/banking-system/bin/activate
```

#### 3. Install the required packages from the requirements file

```
$ pip install -r requirements.txt
```
#### 4. Start the project with an environment variable set 
```
$ RTE=dev python manage.py runserver
```
---
### To run the project locally 
---

#### 1. Download Python from the Python website

#### 2. Install xcode command line tools (if you don't already have them)

```
$ xcode-select --install
```

#### 3. Create a py-env folder in your home folder

```
$ mkdir ~/py-env
$ python3 -m venv py-env/banking-system
```

#### 4. Activate the environment from the project folder

```
$ source ~/py-env/banking-system/bin/activate
```

#### 5. Install the required packages from the requirements file

```
$ pip install -r requirements.txt
```
#### 6. Start the project 
```
$ RTE=dev python manage.py runserver
```

