---
headless: true
---

- [**Example Site**]({{< relref "/docs/example" >}})
- [Project setup]({{< relref "/docs/example/project-setup" >}})
  - [Getting started]({{< relref "/docs/example/project-setup/getting-started" >}})
<br />

- **Shortcodes**
- [Buttons]({{< relref "/docs/shortcodes/buttons" >}})
- [Columns]({{< relref "/docs/shortcodes/columns" >}})
- [Expand]({{< relref "/docs/shortcodes/expand" >}})
- [Hints]({{< relref "/docs/shortcodes/hints" >}})
- [KaTex]({{< relref "/docs/shortcodes/katex" >}})
- [Mermaid]({{< relref "/docs/shortcodes/mermaid" >}})
- [Tabs]({{< relref "/docs/shortcodes/tabs" >}})
<br />
