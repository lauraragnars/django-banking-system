# mqp==5.1.1
asgiref==3.5.0
async-timeout==4.0.2
backports.zoneinfo==0.2.1
billiard==3.6.4.0
black==22.3.0
CacheControl==0.12.11
celery==5.2.7
certifi==2021.10.8
charset-normalizer==2.0.12
click==8.1.3
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.2.0
coverage==6.4.1
cyclonedx-python-lib==2.4.0
Deprecated==1.2.13
dj-rest-auth==2.2.4
Django==4.0.4
django-celery-beat==2.3.0
django-enum-choices==2.1.4
django-environ==0.8.1
django-rest-auth==0.9.5
django-rest-framework==0.1.0
django-rq==2.5.1
django-timezone-field==5.0
djangorestframework==3.13.1
gunicorn==20.1.0
h11==0.13.0
html5lib==1.1
httptools==0.4.0
idna==3.3
kombu==5.2.4
lockfile==0.12.2
msgpack==1.0.4
mypy-extensions==0.4.3
packageurl-python==0.9.9
packaging==21.3
pathspec==0.9.0
pip-api==0.0.29
pip_audit==2.3.1
plan==0.5
platformdirs==2.5.2
progress==1.6
prompt-toolkit==3.0.29
psycopg2==2.9.3
PyJWT==2.4.0
pyparsing==3.0.9
PyQRCode==1.2.1
python-crontab==2.6.0
python-dateutil==2.8.2
pytz==2022.1
redis==4.3.1
requests==2.27.1
resolvelib==0.8.1
rq==1.10.1
six==1.16.0
sqlparse==0.4.2
toml==0.10.2
tomli==2.0.1
twilio==7.8.2
types-setuptools==57.4.17
types-toml==0.10.7
typing_extensions==4.2.0
tzdata==2022.1
urllib3==1.26.9
uvicorn==0.17.6
uvloop==0.16.0
vine==5.0.0
wcwidth==0.2.5
webencodings==0.5.1
websockets==10.3
wrapt==1.14.1
