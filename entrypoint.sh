#!/bin/sh
echo "starting entrypoint script"
echo "** RTE mode: $RTE"
python manage.py check
python manage.py makemigrations
python manage.py migrate

case "$RTE" in
    dev )
        echo "** Development mode"
        pip-audit
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report
        python manage.py runserver 0.0.0.0:8000
        ;;
    test )
        echo "** Test mode"
        pip-audit || exit 1
        pip install coverage
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report -m --fail-under=70
        ;;
    prod )
        echo "** Production mode"
        pip-audit || exit 1
        python manage.py check --deploy
        python manage.py collectstatic --noinput
        gunicorn project.asgi:application -b 0.0.0.0:8080 -k uvicorn.workers.UvicornWorker
        ;;
esac


