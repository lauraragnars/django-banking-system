from django.shortcuts import render, reverse
from .models import PasswordResetRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.http import HttpResponseRedirect
import environ
from twilio.rest import Client
import pyqrcode
import django_rq
from .messaging import email_message
import uuid


def index(request):
    return render(request, "login/index.html")


def login(request):
    context = {}

    if request.method == "POST":
        if "login" in request.POST:
            try:
                user = authenticate(
                    request,
                    username=request.POST["user"],
                    password=request.POST["password"],
                )

                return HttpResponseRedirect(
                    reverse("login:authenticate_user", kwargs={"user": user.pk})
                )
            except Exception:
                context = {"error": "Bad username or password."}

    return render(request, "login/login.html", context)


def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse("login:login"))


def sign_up(request):
    context = {}
    env = environ.Env()
    environ.Env.read_env()
    account_sid = env("TWILIO_ACCOUNT_SID")
    auth_token = env("TWILIO_AUTH_TOKEN")
    service_sid = env("TWILIO_SERVICE_SID")
    client = Client(account_sid, auth_token)

    if request.method == "POST":
        if "signup" in request.POST:
            password = request.POST["password"]
            confirm_password = request.POST["confirm_password"]
            user_name = request.POST["user"]
            phone_number = request.POST["phone_number"]
            email = request.POST["email"]

            if password == confirm_password:
                enitity_id = uuid.uuid4()
                user = User.objects.create_user(user_name, email, password)
                user.profile.phone_number = phone_number
                user.is_staff = True if request.POST["is_staff"] == "yes" else False
                user.profile.topt_identity = enitity_id

                try:
                    user.save()
                    new_factor = (
                        client.verify.services(service_sid)
                        .entities(enitity_id)
                        .new_factors.create(
                            friendly_name=f"{user_name}'s account", factor_type="totp"
                        )
                    )
                    factor_obj = new_factor.binding
                    url = pyqrcode.create(factor_obj["uri"])
                    url.svg("login/static/login/authy-url.svg", scale=2)

                    return HttpResponseRedirect(
                        reverse("login:add_authentication", kwargs={"user": user.pk})
                    )
                except Exception:
                    context = {
                        "error": "Could not create user account - please try again."
                    }
            else:
                context = {"error": "Passwords did not match. Please try again."}

    return render(request, "login/sign_up.html", context)


def add_authentication(request, user):
    context = {}
    user = User.objects.get(pk=user)
    env = environ.Env()
    environ.Env.read_env()
    account_sid = env("TWILIO_ACCOUNT_SID")
    auth_token = env("TWILIO_AUTH_TOKEN")
    service_sid = env("TWILIO_SERVICE_SID")
    client = Client(account_sid, auth_token)

    if request.method == "POST":
        if "register_authenticate" in request.POST:
            register_topt_code = request.POST["register_topt_code"]
            print(register_topt_code)
            factors = (
                client.verify.services(service_sid)
                .entities(user.profile.topt_identity)
                .factors.list(limit=20)
            )

            for record in factors:
                user_factor = record.sid

            factor = (
                client.verify.services(service_sid)
                .entities(user.profile.topt_identity)
                .factors(user_factor)
                .update(auth_payload=register_topt_code)
            )

            if factor.status == "verified":
                return HttpResponseRedirect(reverse("login:login"))
            else:
                context = {"error": "Wrong code, please try again"}

    return render(request, "login/add_authentication.html", context)


def authenticate_user(request, user):
    context = {}
    user = User.objects.get(pk=user)
    env = environ.Env()
    environ.Env.read_env()
    account_sid = env("TWILIO_ACCOUNT_SID")
    auth_token = env("TWILIO_AUTH_TOKEN")
    service_sid = env("TWILIO_SERVICE_SID")
    client = Client(account_sid, auth_token)

    factors = (
        client.verify.services(service_sid)
        .entities(user.profile.topt_identity)
        .factors.list(limit=20)
    )

    for record in factors:
        user_factor = record.sid

    if request.method == "POST":
        if "authenticate" in request.POST:
            topt_code = request.POST["topt_code"]

            challenge = (
                client.verify.services(service_sid)
                .entities(user.profile.topt_identity)
                .challenges.create(auth_payload=topt_code, factor_sid=user_factor)
            )

            if challenge.status == "approved":
                dj_login(request, user)
                return HttpResponseRedirect(
                    reverse(
                        "bank:employee_index"
                        if request.user.is_staff is True
                        else "bank:index"
                    )
                )
            else:
                context = {"error": "Wrong code, please try again"}

    return render(request, "login/authenticate.html", context)


def request_password_reset(request):
    if request.method == "POST":
        post_user = request.POST["username"]
        user = None

        if post_user:
            try:
                user = User.objects.get(username=post_user)
            except Exception:
                print(f"Invalid password request: {post_user}")
        else:
            post_user = request.POST["email"]
            try:
                user = User.objects.get(email=post_user)
            except Exception:
                print(f"Invalid password request: {post_user}")
        if user:
            prr = PasswordResetRequest()
            prr.user = user
            prr.save()
            django_rq.enqueue(
                email_message,
                {
                    "token": prr.token,
                    "email": prr.user.email,
                },
            )
            return HttpResponseRedirect(reverse("login:password_reset"))

    return render(request, "login/request_password_reset.html")


def password_reset(request):
    if request.method == "POST":
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        token = request.POST["token"]

        if password == confirm_password:
            try:
                prr = PasswordResetRequest.objects.get(token=token)
                prr.save()
            except Exception:
                print("Invalid password reset attempt.")
                return render(request, "login/password_reset.html")

            user = prr.user
            user.set_password(password)
            user.save()
            return HttpResponseRedirect(reverse("login:login"))

    return render(request, "login/password_reset.html")
