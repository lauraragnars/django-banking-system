from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
import uuid


class LoginTestCase(TestCase):
    def setUp(self):
        User(username="user", email="test@test.is", password="testPassword").save()

    # Sign up
    def test_sign_up(self):
        password = "testPassword"
        confirm_password = "testPassword"
        user_name = "testUser"
        phone_number = "42326324"
        email = "test@test.com"

        if password == confirm_password:
            enitity_id = uuid.uuid4()
            user = User.objects.create_user(user_name, email, password)
            user.profile.phone_number = phone_number
            user.is_staff = False
            user.profile.topt_identity = enitity_id
            user.save()

            print("The test user has been created")

        else:
            print("The passwords did not match")


class SigninTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testUser", password="password", email="test@example.com"
        )
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username="testUser", password="password")
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username="wrong", password="password")
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username="testUser", password="wrong")
        self.assertFalse(user is not None and user.is_authenticated)
