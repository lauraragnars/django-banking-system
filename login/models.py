from xmlrpc.client import boolean
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models.query import QuerySet
from bank.models import Account, Ledger
from secrets import token_urlsafe


class Profile(models.Model):
    BASIC = "basic"
    SILVER = "silver"
    GOLD = "gold"

    RANKS = [
        (BASIC, "Basic"),
        (SILVER, "Silver"),
        (GOLD, "Gold"),
    ]

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.IntegerField(default=12345)
    customer_rank = models.CharField(choices=RANKS, default=BASIC, max_length=6)
    topt_identity = models.CharField(max_length=64, null=True)

    @property
    def full_name(self) -> str:
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def is_customer(self) -> boolean:
        return self.user.is_staff

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()

    @property
    def can_take_loan(self) -> boolean:
        if self.customer_rank == "silver" or self.customer_rank == "gold":
            return True
        else:
            return False

    @classmethod
    def take_loan(cls, customer, credit_account, credit_text, amount, loan_name):
        assert amount >= 0, "Negative amount not allowed for loan."
        assert cls.can_take_loan, "User rank does not allow for making loans."

        loan_account = Account.objects.create(
            user=customer, account_name=loan_name, is_loan=True
        )

        Ledger.transfer(
            amount=amount,
            debit_account=loan_account,
            debit_text="New loan",
            credit_account=credit_account,
            credit_text=credit_text,
            is_loan=True,
        )

    def __str__(self):
        return f"{self.user.username}"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class PasswordResetRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=43, default=token_urlsafe)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user} - {self.created_timestamp} - {self.updated_timestamp} - {self.token}"
