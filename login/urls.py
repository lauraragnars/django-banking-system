from django.urls import path
from . import views

app_name = "login"

urlpatterns = [
    path("", views.index, name="index"),
    path("login/", views.login, name="login"),
    path("sign_up/", views.sign_up, name="sign_up"),
    path("logout/", views.logout, name="logout"),
    path(
        "request_password_reset/",
        views.request_password_reset,
        name="request_password_reset",
    ),
    path("password_reset/", views.password_reset, name="password_reset"),
    path(
        "add_authentication/<int:user>/",
        views.add_authentication,
        name="add_authentication",
    ),
    path(
        "authenticate_user/<int:user>/",
        views.authenticate_user,
        name="authenticate_user",
    ),
]
