from django.contrib import admin
from .models import Profile
from .models import PasswordResetRequest

admin.site.register(Profile)
admin.site.register(PasswordResetRequest)
